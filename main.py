import urllib.request
import cv2
import numpy as np
from resolution import get_resolution
from stream import streem1
path='/Users/viktor/PycharmProjects/binarization_ocy/firstttt_frame.jpg' #path to first frame
img = streem1()
resolution=get_resolution(path)
w=resolution[0]
h=resolution[1]
y=int(h/2)
x=0

url = 'http://10.45.217.56:8080/shot.jpg'

while True:
    imgResp = urllib.request.urlopen(url)
    imgNp = np.array(bytearray(imgResp.read()), dtype=np.uint8)
    img = cv2.imdecode(imgNp, -1)
    frame = img[y:y + h, x:x + w]
    cv2.imshow('IPWebcam', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
